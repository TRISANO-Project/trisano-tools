import argparse
import csv
import logging
from abc import ABC, abstractmethod
from collections import defaultdict

import requests
from bs4 import BeautifulSoup
from fuzzywuzzy import process

logging.basicConfig(filename='trisano_form_disease_load.log', level=logging.INFO, filemode='w')

HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:57.0) Gecko/20100101 Firefox/57.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1'
}


class PopulateDiseaseForms(object):
    def __init__(self, csv):
        self.url_base = "http://{domain}/forms".format(domain=HEADERS.get("Host", None))
        self.csv_path = csv
        self.form_definition = None
        self.disease_definition = None
        self.form_disease_map = None
        self.failed_forms = {}

    def get_form_definition(self):
        logging.info("Getting Form Definitions")
        self.form_definition = self.bootstrap_from_page(url=self.url_base, obj=FormDefinition)
        logging.info("Got {} form definitions".format(len(self.form_definition.apis)))

    def get_disease_definition(self):
        if self.form_definition is None:
            self.get_form_definition()
        logging.info("Getting Disease Definitions")
        self.disease_definition = self.bootstrap_from_page(
            url='{base}/{id}/edit'.format(base=self.url_base, id=list(self.form_definition.apis.values())[0].id),
            obj=DiseaseDefinition)
        logging.info("Got {} disease definitions".format(len(self.disease_definition.apis)))

    def get_form_disease_map(self):
        logging.info("Loading CSV")
        f = FormDiseaseMap(self.csv_path)
        self.form_disease_map = f.form_disease_map
        logging.info("Loaded {} forms from csv".format(len(self.form_disease_map)))

    def bootstrap_from_page(self, url=None, obj=None):
        return obj(self.make_request(url, requests.get))

    def make_request(self, url=None, action=None, headers=None, data=None):
        action_params = {'headers': HEADERS if headers is None else headers}
        if data is not None and action is requests.post:
            action_params['data'] = data
        r = action(url=url, **action_params)
        if r.status_code != 200:
            raise Exception(r.text)
        return r.text

    def populate(self):
        data = (
            ("_method", "put"),
            ("form[jurisdiction_id]", ""),
            ("form[description]", ""),
            ("commit", "Update"),
        )
        for form, diseases in self.form_disease_map.items():
            logging.info("Posting form {}".format(form))
            disease_ids = self.get_disease_ids(diseases)
            form_match = self.form_definition.get_fuzzy_match(form)
            if form_match is None:
                continue
            logging.info(
                "Posting form {} with id {}, {} disease with ids: {}".format(form, form_match.id, len(diseases),
                                                                             ",".join(disease_ids)))
            try:
                self.post_form_diseases(form_match.id, disease_ids, data)
            except:
                self.failed_forms[form] = {"diseases": diseases, "ids": disease_ids}

    def get_disease_ids(self, names):
        ids = [getattr(self.disease_definition.get_fuzzy_match(name), 'id', None) for name in names]
        ids = set(filter(lambda x: x is not None, ids))
        return ids

    def post_form_diseases(self, form_id, disease_ids, general_data):
        url = "{base}/{id}".format(base=self.url_base, id=form_id)
        referer = "{url}/edit".format(url=url)
        headers = HEADERS.copy()
        headers['Referer'] = referer
        disease_data = ()
        for disease_id in disease_ids:
            disease_data += (('form[disease_ids][]', disease_id),)
        self.make_request(url=url, action=requests.post, headers=headers, data=general_data + disease_data)

    def run(self):
        self.get_form_definition()
        self.get_disease_definition()
        self.get_form_disease_map()
        self.populate()


class FormDiseaseMap(object):
    def __init__(self, file_loc=None):
        self.form_disease_map = defaultdict(list)
        self.file_location = file_loc
        self.load_csv()

    def load_csv(self):
        with open(self.file_location) as f:
            reader = csv.DictReader(f, fieldnames=('form', 'disease'))
            for row in reader:
                self.form_disease_map[row['form']].append(row['disease'])


class DefinitionObject(ABC):
    definition_object_type = None

    def __init__(self, html):
        super(DefinitionObject, self).__init__()
        self.soup = BeautifulSoup(html, 'html.parser')
        self.apis = {}
        self.get_apis()

    @abstractmethod
    def get_apis(self):
        pass

    def get_fuzzy_match(self, name):
        match = process.extractBests(name, self.apis.keys())
        if match[0][1] < 90:
            FS.add_bad_score((name, match[0]))
            match_obj = None
        else:
            match_obj = self.apis.get(match[0][0], None)
            if match[0][1] < 100:
                FS.add_ok_score((name, match[0]))
            else:
                FS.add_good_score((name, match[0]))

        if match_obj is None:
            logging.warning("{} could not match name {}".format(self.definition_object_type, name))
        else:
            match_obj.fuzzy_match = name
        return match_obj

    def populate_apis(self, obj, nodes):
        for node in nodes:
            o = obj(node)
            self.apis[o.name] = o


class FormDefinition(DefinitionObject):
    definition_object_type = "Form"

    def get_apis(self):
        self.populate_apis(Form, [node for node in self.soup("a") if
                                  "/forms/builder" in node.get("href") and not node.contents == ['Builder']])


class DiseaseDefinition(DefinitionObject):
    definition_object_type = "Disease"

    def get_apis(self):
        self.populate_apis(Disease,
                           [node for node in self.soup.find_all('input') if node.get("name") == "form[disease_ids][]"])


class APIObject(ABC):
    def __init__(self, node):
        self.node = node
        self.id = None
        self.name = None
        self.fuzzy_match = None
        super(APIObject, self).__init__()
        self.parse_node(node)

    @abstractmethod
    def parse_node(self, node):
        pass


class Form(APIObject):
    def parse_node(self, node):
        self.id = node.get("href").replace('/forms/builder/', '')
        self.name = node.contents[0]


class Disease(APIObject):
    def parse_node(self, node):
        self.id = node.get("value")
        self.name = node.next_sibling


class FuzzyScore(object):
    def __init__(self):
        self.scores = {"Good": [], "OK": [], "Bad": []}

    def add_score(self, target, score):
        self.scores[target].append(score)

    def add_good_score(self, score):
        self.add_score("Good", score)

    def add_ok_score(self, score):
        self.add_score("OK", score)

    def add_bad_score(self, score):
        self.add_score("Bad", score)

    def get_map(self, level):
        return [(x[0], x[1][0]) for x in set(self.scores.get(level))]


FS = FuzzyScore()


def main():
    parser = argparse.ArgumentParser(description="Setup trisano form/disease mappings")
    parser.add_argument("csv", help="Path to csv file containing the forms to diseases mapping.")
    parser.add_argument("domain", help="Trisano domain, without a protocol")
    parser.add_argument("auth_cookies", help="Capture the cookies after logging in and put them here")
    args = parser.parse_args()

    set_headers(args.domain, args.auth_cookies)
    run(args.csv)


def run(csv_file):
    populate_disease_forms = PopulateDiseaseForms(csv_file)
    populate_disease_forms.run()
    return populate_disease_forms


def set_headers(host, cookie):
    HEADERS['Host'] = host
    HEADERS['Cookie'] = cookie


if __name__ == "__main__":
    main()
